import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BelajarNavBar(),
    );
  }
}

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class TeksUtama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Text(
          'saya belajar flutter di mobile Programming 2',
          maxLines: 1,
        ),
        Text(""),
        Text(
          'saya belajar flutter di mobile Programming 2',
          overflow: TextOverflow.ellipsis,
        ),
        Text(""),
        Text(
          'saya belajar flutter di mobile Programming 2',
          overflow: TextOverflow.clip,
        ),
        Text(""),
        Text(
          'saya belajar flutter di mobile Programming 2',
          textAlign: TextAlign.center,
        ),
        Text(""),
        Text(
          'saya belajar flutter di mobile Programming 2',
          style: TextStyle(
              color: Colors.blue,
              fontStyle: FontStyle.italic,
              fontSize: 20,
              fontWeight: FontWeight.w700),
        ),
      ],
    );
  }
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Konsep State, Stateless, Stateful dan widgets"),
      ),
      body: Center(
        child: Container(
          width: 150,
          height: 250,
          child: TeksUtama(),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Beranda'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            title: Text('Pesanan'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            title: Text('Inbox'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Akun'),
          ),
        ],
        currentIndex: 0,
        selectedItemColor: Colors.green,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
      ),
    );
  }
}
